pfUI:RegisterModule("raid", "vanilla:tbc", function ()
  -- do not go further on disabled UFs
  if C.unitframes.disable == "1" then return end

  local default_border = C.appearance.border.default
  if C.appearance.border.unitframes ~= "-1" then
    default_border = C.appearance.border.unitframes
  end

  pfUI.uf.raid = CreateFrame("Button","pfRaid",UIParent)

  pfUI.uf.raid.tanksfirst = {
    ["PF_TANK_TOGGLE"] = { T["Toggle as Tank"], "toggleTank" }
  }

  local tank_token = L["tokens"]["Tank"]
  local tank_replace = string.gsub(string.gsub(tank_token,"%[","%%["),"%]","%%]")
  local player = UnitName("player")

  HookAddonOrVariable("Blizzard_RaidUI", function()
    hooksecurefunc("RaidGroupFrame_Update",pfUI.uf.raid.Blizzard_RaidUI, true)
  end)

  -- no tank order for now, just "all tanks first"
  pfUI.uf.raid.tankrole = { }

  function pfUI.uf.raid:UpdateConfig()
    for i=1, 40 do
      pfUI.uf.raid[i]:UpdateConfig()
    end
  end

  for r=1, 8 do
    for g=1, 5 do
      local i = g + 5*(r-1)
      pfUI.uf.raid[i] = pfUI.uf:CreateUnitFrame("Raid", i, C.unitframes.raid)
      pfUI.uf.raid[i]:UpdateFrameSize()

      local spacing = pfUI.uf.raid[i].config.pspace
      local width = pfUI.uf.raid[i].config.width
      local height = pfUI.uf.raid[i].config.height
      local pheight = pfUI.uf.raid[i].config.pheight
      local real_height = height + spacing + pheight + 2*default_border

      pfUI.uf.raid[i]:SetPoint("BOTTOMLEFT", (r-1) * (width+3*default_border) + 5, C.chat.left.height + default_border + 10 + (g-1)*(real_height+3*default_border))
      UpdateMovable(pfUI.uf.raid[i])
    end
  end

  -- add units to the beginning of their groups
  function pfUI.uf.raid:AddUnitToGroup(index, group)
    for subindex = 1, 5 do
      local ids = subindex + 5*(group-1)
      if pfUI.uf.raid[ids].id == 0 and pfUI.uf.raid[ids].config.visible == "1" then
        pfUI.uf.raid[ids].id = index
        pfUI.uf.raid[ids]:Show()
        pfUI.uf:RefreshUnit(pfUI.uf.raid[ids], "all")
        break
      end
    end
  end

  function pfUI.uf.raid:RaidSetup()
    
    if not UnitInRaid("player") then -- disbanded
      for name in pairs(pfUI.uf.raid.tankrole) do
        pfUI.uf.raid.tankrole[name] = false
      end
      if pfUI.uf.player then
        pfUI.uf:RefreshUnit(pfUI.uf.player, "all")
      end
    end

    for i=1, 40 do
      pfUI.uf.raid[i].id = 0
      pfUI.uf.raid[i]:Hide()
    end

    -- sort tanks into their groups
    for i=1, GetNumRaidMembers() do
      local name, _, subgroup  = GetRaidRosterInfo(i)
      if name and pfUI.uf.raid.tankrole[name] then
        pfUI.uf.raid:AddUnitToGroup(i, subgroup)
      end
    end

    -- sort players into roster
    for i=1, GetNumRaidMembers() do
      local name, _, subgroup  = GetRaidRosterInfo(i)
      if name and not pfUI.uf.raid.tankrole[name] then
        pfUI.uf.raid:AddUnitToGroup(i, subgroup)
      end
    end

    self:Blizzard_RaidUI()
  end

  function pfUI.uf.raid:Blizzard_RaidUI()
    -- update blizzard raidframe with our tank tag
    for i=1, MAX_RAID_MEMBERS do
      local button = getglobal("RaidGroupButton"..i)
      local name = button and button.name
      if type(name)=="string" then
        local buttonRank = getglobal("RaidGroupButton"..i.."Rank")
        if buttonRank and buttonRank.GetText then
          local rankText = buttonRank:GetText() or ""
          local is_tank = pfUI.uf.raid.tankrole[name]
          if rankText == "" then
            if is_tank then
              rankText = tank_token
            end
          else -- if they have a rank we'll just swap parenthesis for brackets to avoid text truncation
            if is_tank then
              rankText = string.gsub(string.gsub(rankText,"%(","%["),"%)","%]")
            else
              rankText = string.gsub(string.gsub(string.gsub(rankText,tank_replace,""),"%]","%)"),"%[","%(")
            end
          end
          buttonRank:SetText(rankText)            
        end
      end
    end    
  end

  pfUI.uf.raid:Hide()
  pfUI.uf.raid:RegisterEvent("RAID_ROSTER_UPDATE")
  pfUI.uf.raid:RegisterEvent("VARIABLES_LOADED")
  pfUI.uf.raid:SetScript("OnEvent", function() 
    pfUI.uf.raid:RaidSetup() 
  end)
  pfUI.uf.raid.comms = CreateFrame("Frame")
  pfUI.uf.raid.comms:RegisterEvent("CHAT_MSG_ADDON")
  pfUI.uf.raid.comms:SetScript("OnEvent", function()
    -- accept MTs from oRA2/CTRaidAssist
    if arg1 == "CTRA" then --[[and arg4 ~= player]] 
      local need_update
      local _, _, num, name = string.find(arg2, "^SET (%d+) (.+)$")
      if num and name then
        if not pfUI.uf.raid.tankrole[name] then
          pfUI.uf.raid.tankrole[name] = true
          need_update = true
        end
      else
        _, _, name = string.find( arg2, "^R (.+)$")
        if name then
          if pfUI.uf.raid.tankrole[name] then
            pfUI.uf.raid.tankrole[name] = false
            need_update = true
          end
        end
      end
      if need_update then
        pfUI.uf.raid:RaidSetup()
      end
    end
  end)

  -- raid popup option to toggle tank role
  local iupm = table.getn(UnitPopupMenus["RAID"])
  for label, data in pairs(pfUI.uf.raid.tanksfirst) do
    UnitPopupButtons[label] = { text = TEXT(data[1]), dist = 0 }
    table.insert(UnitPopupMenus["RAID"], iupm-1, label)
  end


  hooksecurefunc("UnitPopup_OnClick", function()
    local dropdownFrame = _G[UIDROPDOWNMENU_INIT_MENU]
    local button = this.value
    local unit = dropdownFrame.unit
    local name = dropdownFrame.name

    if button and pfUI.uf.raid.tanksfirst[button] and name then
      pfUI.uf.raid.tankrole[name] = not pfUI.uf.raid.tankrole[name]
      -- fire off a message to CTRaidAssist / oRA2
      local is_tank = pfUI.uf.raid.tankrole[name]
      if ( IsRaidLeader() or IsRaidOfficer() ) then
        if (is_tank) then
          local index = 0
          for _,status in pairs(pfUI.uf.raid.tankrole) do
            if (status) then
              index = index + 1
            end
          end
          if index > 0 then 
            SendAddonMessage("CTRA", string.format("SET %d %s",index,name), "RAID")
          end
        else
          SendAddonMessage("CTRA", string.format("R %s",name), "RAID")
        end
      end
      pfUI.uf.raid:RaidSetup()
    end
  end)
end)
